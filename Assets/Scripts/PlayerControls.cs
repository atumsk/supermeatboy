﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private bool Right = true;
    public float maxS = 5f;

    Animator anim;

    bool grounded = false;
    bool dead = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;
    public float jumpForce = 300f;

    public AudioSource jumpSound;
    public AudioSource deathSound;

    bool doubleJump = false;

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        anim.SetBool("Ground", grounded);
        anim.SetBool("Dead", dead);

        if (grounded)
            doubleJump = false;

        anim.SetFloat("vSpeed", rb2d.velocity.y);


        move = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(move));

        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

        if (move > 0 && !Right)
            Flip();
        else if (move < 0 && Right)
            Flip();
    }

    void Update()
    {
        if ((grounded || !doubleJump) && Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("Ground", false);
            rb2d.AddForce(new Vector2(0, jumpForce));

            jumpSound.Play();

            if (!doubleJump && !grounded)
                doubleJump = true;

        }
    }

    void Flip()
    {
        Right = !Right;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "KillBox")
        {
            deathSound.Play();
            anim.SetBool("Dead", true);
        }
    }
}